FROM node:lts-alpine as build-deps

ENV HOME=/home/app/

COPY . $HOME
WORKDIR $HOME

COPY package.json $HOME
RUN yarn 
COPY . $HOME
RUN yarn run build

FROM nginx:1.12-alpine
COPY --from=build-deps /home/app/build /usr/share/nginx/html
COPY --from=build-deps /home/app/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]