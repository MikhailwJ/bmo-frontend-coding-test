# BMO Frontend coding test

As a user running the application
I can view a list of restaurants in a user submitted City (e.g. Toronto)
So that I know which restaurants are currently available

# Technical questions

Please answer the following questions in a markdown file called Answers to technical questions.md.

- How long did you spend on the coding test?

  About 6-8 hours.

- What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.

  1. Clear the Refine box when a new city get searched.
  2. Use a better fuzzy search or implement my own.
  3. Make the ui nicer by adding more custom css and animations.
  4. A map with pins of all the locations.

- What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.

  ```js
  Array.flat();
  ```

  ```js
  const restaurants = [
    ...items,
    [...addresses].map(add => ({ name: add, type: 'address' })),
    [...areas].map(add => ({ name: add, type: 'area' }))
  ].flat();
  ```

- How would you track down a performance issue in production? Have you ever had to do this?
  yes, chrome dev tools, react debugging and React dev tools.

  1. Identify wasted renders.
  2. Utilize tree-shakeable components or find other means of minifing packages.
  3. Make code more DRY.
  4. Make sure webpack is code splitting to avoid a large bundle being sent to the client.

- How would you improve the API that you just used?

  1. Add categories to the restaurants such as: dinner, breakfast, itallian etc...
  2. Add hours of operation.
  3. Ratings/votes.

- Please describe yourself using JSON.

  ```
  {
    "firstName": "Mikhail",
    "lastName": "Johnson",
    "age": 28,
    "nationality": "African-American",
    "livesin": "Toronto (GTA)",
    "passions": ["Programming", "Video games", "Teaching others", "Driving"],
    "interests": ["Education", "Technology", "Flying", "Tacos"],
    "dreams": ["To make this world a better place!"],
    "believes": ["Anything is possible"]
   }

  ```
