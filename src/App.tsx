import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import { HomeComponent } from './components/HomeComponent';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={HomeComponent} exact />
        <Route path="/:location" component={HomeComponent} />
        <Route />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
