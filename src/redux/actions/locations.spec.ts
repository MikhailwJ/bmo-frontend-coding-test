import { requestCities, requestCountries, recivedCities, recivedCountries } from './locations';
import { FETCH_CITIES, FETCH_COUNTRIES, RECIVED_CITIES, RECIVED_COUNTRIES } from '../constants/actions';

describe('Location Actions', () => {
  it('requestCities', () => {
    expect(requestCities()).toStrictEqual({ type: FETCH_CITIES });
  });
  it('requestCountries', () => {
    expect(requestCountries()).toStrictEqual({ type: FETCH_COUNTRIES });
  });
  it('recivedCities', () => {
    const cities = { count: 3, cities: ['Chicago', 'San Francisco', 'New York'] };
    expect(recivedCities(cities)).toStrictEqual({ type: RECIVED_CITIES, data: cities });
  });
  it('recivedCountries', () => {
    const countries = { count: 2, countries: ['CA', 'US'] };
    expect(recivedCountries(countries)).toStrictEqual({ type: RECIVED_COUNTRIES, data: countries });
  });
});
