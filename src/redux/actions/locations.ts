import { RECIVED_CITIES, RECIVED_COUNTRIES, FETCH_CITIES, FETCH_COUNTRIES } from '../constants/actions';
import { LocationDTO } from '../../types';

export const requestCities = () => ({ type: FETCH_CITIES });
export const requestCountries = () => ({ type: FETCH_COUNTRIES });
export const recivedCities = (data: LocationDTO) => ({ type: RECIVED_CITIES, data });
export const recivedCountries = (data: LocationDTO) => ({ type: RECIVED_COUNTRIES, data });
