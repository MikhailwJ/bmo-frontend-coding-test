import { FILTER_RESTAURANTS, FETCH_RESTAURANTS, RECIVED_RESTAURANTS } from '../constants/actions';
import { requestRestaurants, recivedRestaurants, filterRestaurants } from './restauraunts';

describe('Restaurant Actions', () => {
  it('requestRestaurants', () => {
    const city = 'toronto';
    expect(requestRestaurants(city)).toStrictEqual({ type: FETCH_RESTAURANTS, city: city });
  });

  it('recivedRestaurants', () => {
    const data = {
      count: 1,
      per_page: 1,
      current_page: 1,
      restaurants: [
        {
          id: 107257,
          name: 'Las Tablas Colombian Steak House',
          address: '2942 N Lincoln Ave',
          city: 'Chicago',
          state: 'IL',
          area: 'Chicago / Illinois',
          postal_code: '60657',
          country: 'US',
          phone: '7738712414',
          lat: 41.935137,
          lng: -87.662815,
          price: 2,
          reserve_url: 'http://www.opentable.com/single.aspx?rid=107257',
          mobile_reserve_url: 'http://mobile.opentable.com/opentable/?restId=107257',
          image_url: 'https://www.opentable.com/img/restimages/107257.jpg'
        }
      ]
    };
    expect(recivedRestaurants(data)).toStrictEqual({ type: RECIVED_RESTAURANTS, data });
  });

  it('filterRestaurants', () => {
    const location = { name: 'Toronto / SW Ontario', type: 'name' as 'name' };
    expect(filterRestaurants(location)).toStrictEqual({ type: FILTER_RESTAURANTS, location });
  });
});
