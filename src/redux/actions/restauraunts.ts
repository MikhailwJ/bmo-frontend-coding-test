import { FETCH_RESTAURANTS, RECIVED_RESTAURANTS, FILTER_RESTAURANTS } from '../constants/actions';
import { SelectedLocation, RestaurantDTO } from '../../types';

export const requestRestaurants = (city: string) => ({ type: FETCH_RESTAURANTS, city });
export const recivedRestaurants = (data: RestaurantDTO) => ({ type: RECIVED_RESTAURANTS, data });
export const filterRestaurants = (location: SelectedLocation) => ({ type: FILTER_RESTAURANTS, location });
