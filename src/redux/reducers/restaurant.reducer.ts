import { FETCH_RESTAURANTS, RECIVED_RESTAURANTS, FILTER_RESTAURANTS } from '../constants/actions';

const initState = { count: 0, fetching: false, items: [] };

export const restaurantReducer = (state = initState, action: any) => {
  const data = action.data;
  switch (action.type) {
    case FETCH_RESTAURANTS:
      return { ...state, fetching: true, filtered: undefined };
    case RECIVED_RESTAURANTS:
      return {
        ...state,
        fetching: false,
        count: data.count,
        items: data.restaurants.map(({ id, name, address, city, area, postal_code, phone, price, image_url }) => ({
          id,
          name,
          address,
          city,
          area,
          postal_code,
          phone,
          price,
          image_url
        }))
      };
    case FILTER_RESTAURANTS:
      const { location } = action;
      const filtered = state.items.filter(item => item[location.type] === location.name);
      return {
        ...state,
        filtered: filtered.length > 0 ? filtered : undefined
      };
    default:
      return state;
  }
};
