import { RECIVED_CITIES, FETCH_CITIES, FETCH_COUNTRIES, RECIVED_COUNTRIES } from '../constants/actions';
import { getName, registerLocale } from 'i18n-iso-countries';
registerLocale(require('i18n-iso-countries/langs/en.json'));

const initState = { count: 0, fetching: false, items: [] };

const cities = (state = initState, action: any) => {
  const data = action.data;
  switch (action.type) {
    case FETCH_CITIES:
      return { ...state, fetching: true };
    case RECIVED_CITIES:
      return { ...state, fetching: false, count: data.count, items: data.cities.map((city: string) => ({ name: city })) };
    default:
      return state;
  }
};

const countries = (state = initState, action: any) => {
  const data = action.data;
  switch (action.type) {
    case FETCH_COUNTRIES:
      return { ...state, fetching: true };
    case RECIVED_COUNTRIES:
      return { ...state, fetching: false, count: data.count, items: data.countries.map(code => ({ code, name: getName(code, 'en') })) };
    default:
      return state;
  }
};

export const locationReducer = { cities, countries };
