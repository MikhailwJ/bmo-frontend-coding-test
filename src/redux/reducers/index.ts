import { combineReducers } from 'redux';
import { locationReducer } from './locations.reducer';
import { restaurantReducer as restaurants } from './restaurant.reducer';

export const rootReducer = combineReducers({ ...locationReducer, restaurants });
