import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootReducer } from './reducers';
import { locationSaga } from './sagas/location.saga';
import { restaurantsSaga } from './sagas/restaurant.saga';

const persistedState = {};
const sagaMiddleware = createSagaMiddleware();
const enhancer = process.env.NODE_ENV === 'production' ? applyMiddleware(sagaMiddleware) : composeWithDevTools(applyMiddleware(sagaMiddleware));

export const store = createStore(rootReducer, persistedState, enhancer);
sagaMiddleware.run(locationSaga);
sagaMiddleware.run(restaurantsSaga);
