import { baseUrl, appendPathToBaseUrl } from './api';

describe('api (constants)', () => {
  const path = 'test';
  it('will take a string with no /', () => {
    expect(appendPathToBaseUrl(path)).toBe(baseUrl + path);
  });

  it('will take a string with a preceding /', () => {
    expect(appendPathToBaseUrl('/' + path)).toBe(baseUrl + path);
  });

  it('will take a string with a postceding /', () => {
    expect(appendPathToBaseUrl('/' + path)).toBe(baseUrl + path);
  });
});
