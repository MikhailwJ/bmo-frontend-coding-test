import join from 'url-join';

export const baseUrl = 'https://opentable.herokuapp.com/api/';
export const appendPathToBaseUrl = (path: string) => join(baseUrl, path);

export const statsEndpoint = appendPathToBaseUrl('stats');
export const citiesEndpoint = appendPathToBaseUrl('cities');
export const countriesEndpoint = appendPathToBaseUrl('countries');
export const restaurantsEndpoint = appendPathToBaseUrl('restaurants');
