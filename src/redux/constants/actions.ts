export const FETCH_STATS = 'FETCH_STATS';
export const FETCH_COUNTRIES = 'FETCH_COUNTRIES';
export const FETCH_CITIES = 'FETCH_CITIES';
export const FETCH_RESTAURANTS = 'FETCH_RESTAURANTS';
export const FETCH_RESTAURANT = 'FETCH_RESTAURANT';
export const FILTER_RESTAURANTS = 'FILTER_RESTAURANTS';

export const RECIVED_STATS = 'RECIVED_STATS';
export const RECIVED_COUNTRIES = 'RECIVED_COUNTRIES';
export const RECIVED_CITIES = 'RECIVED_CITIES';
export const RECIVED_RESTAURANTS = 'RECIVED_RESTAURANTS';
export const RECIVED_RESTAURANT = 'RECIVED_RESTAURANT';
