import Axios from 'axios';
import { call, put, takeLatest } from '@redux-saga/core/effects';
import { citiesEndpoint, countriesEndpoint } from '../constants/api';
import { FETCH_CITIES, FETCH_COUNTRIES } from '../constants/actions';
import { recivedCities, recivedCountries } from '../actions/locations';

export function* cities() {
  const res = yield call(Axios.get, citiesEndpoint);
  yield put(recivedCities(res.data));
}

export function* countries() {
  const res = yield call(Axios.get, countriesEndpoint);
  yield put(recivedCountries(res.data));
}

export function* locationSaga() {
  yield takeLatest(FETCH_CITIES, cities);
  yield takeLatest(FETCH_COUNTRIES, countries);
}
