import Axios from 'axios';
import { AnyAction } from 'redux';
import { call, put, takeLatest } from '@redux-saga/core/effects';
import { FETCH_RESTAURANTS } from '../constants/actions';
import { recivedRestaurants } from '../actions/restauraunts';
import { restaurantsEndpoint } from '../constants/api';

function* restaurants({ city }: AnyAction) {
  const res = yield call(Axios.get, restaurantsEndpoint, { params: { city } });
  yield put(recivedRestaurants(res.data));
}

export function* restaurantsSaga() {
  yield takeLatest(FETCH_RESTAURANTS, restaurants);
}
