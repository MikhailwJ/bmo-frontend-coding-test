export interface IRestaurant {
  id: number;
  name: string;
  address: string;
  city: string;
  state?: string;
  area: string;
  postal_code: string;
  country: string;
  phone: string;
  price: number;
  image_url: string;
  lat?: number;
  lng?: number;
  reserve_url?: string;
  mobile_reserve_url?: string;
}

export interface ICity {
  name: string;
}

export interface ICountry {
  code: string;
  name: string;
}

export interface SelectedLocation {
  name: string;
  type?: 'name' | 'address' | 'area';
}

export interface LocationDTO {
  count: number;
  [key: string]: any;
}
export interface RestaurantDTO {
  count: number;
  per_page: number;
  current_page: number;
  restaurants: IRestaurant[];
}
