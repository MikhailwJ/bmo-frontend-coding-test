export const printDollarSigns = (qty: number) => [1, 2, 3, 4].map(v => (qty >= v ? '$' : ''));
export const removePhoneExtention = (phone: string) => (phone || '').toLowerCase().split('x')[0];
