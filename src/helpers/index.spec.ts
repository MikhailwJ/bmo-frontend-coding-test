import { printDollarSigns, removePhoneExtention } from '.';

describe('Helpers', () => {
  describe('printDollarSigns', () => {
    it('returns an array of empty strings', () => {
      const result = printDollarSigns(0);
      expect(result.every(item => item === '')).toBeTruthy();
    });
    it("returns no more than 4 $'s", () => {
      for (let index = 0; index < 6; index++) {
        const result = printDollarSigns(index);
        expect(result.length === 4 && result.some(item => item === '$' || item === '')).toBeTruthy();
      }
    });
  });

  describe('removePhoneExtention', () => {
    it('removes exerything after X', () => {
      const phone = '416-123-4567x7';
      expect(removePhoneExtention(phone)).toBe('416-123-4567');
    });
  });
});
