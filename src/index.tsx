import * as serviceWorker from './serviceWorker';
import App from './App';
import React from 'react';
import { createMuiTheme, CssBaseline } from '@material-ui/core';
import { Provider } from 'react-redux';
import { red } from '@material-ui/core/colors';
import { render } from 'react-dom';
import { store } from './redux/store';
import './index.css';
import { ThemeProvider } from '@material-ui/styles';

const baseTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#556cd6'
    },
    secondary: {
      main: '#19857b'
    },
    error: {
      main: red.A400
    },
    background: {
      default: '#fff'
    }
  }
});

render(
  <Provider store={store}>
    <ThemeProvider theme={baseTheme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
