import React, { FC } from 'react';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import { Theme } from 'react-autosuggest';

interface HeaderProps {
  title: string;
}

const useStyles = makeStyles((theme: Theme) => ({ root: { flexGrow: 1 }, title: { flexGrow: 1 } }));

export const Header: FC<HeaderProps> = ({ title }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};
