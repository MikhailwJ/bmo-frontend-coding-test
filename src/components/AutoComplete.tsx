import Autosuggest, { SuggestionSelectedEventData } from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import parse from 'autosuggest-highlight/parse';
import React, { ChangeEvent, FC, useState, FormEvent } from 'react';
import TextField from '@material-ui/core/TextField';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Fuse from 'fuse.js';
import { SelectedLocation } from '../types';

interface AutoCompleteProps {
  label: string;
  placeHolder?: string;
  suggestions: any[];
  onSelected: (location: SelectedLocation) => any;
  clearable?: boolean;
}

function renderInputComponent(inputProps: any) {
  const { classes, inputRef = () => {}, ref, ...other } = inputProps;

  return (
    <TextField
      fullWidth
      InputProps={{
        inputRef: node => {
          ref(node);
          inputRef(node);
        },
        classes: { input: classes.input }
      }}
      {...other}
    />
  );
}

function renderSuggestion(suggestion, { query, isHighlighted }: Autosuggest.RenderSuggestionParams) {
  const { name } = suggestion;
  const matches = match(name, query);
  const parts = parse(name, matches);

  return (
    <MenuItem selected={isHighlighted} component="div">
      <div>
        {parts.map((part, i) => {
          return (
            <span key={i + '-' + part.text} style={{ fontWeight: part.highlight ? 500 : 400 }}>
              {part.text}
            </span>
          );
        })}
      </div>
    </MenuItem>
  );
}

function getSuggestions(suggestions: any[], value: string, keys?: string[]) {
  if (value.trim().length === 0) return [];

  const fuse = new Fuse(suggestions, {
    shouldSort: true,
    findAllMatches: true,
    threshold: 0.3,
    location: 10,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 2,
    keys: keys || ['name']
  });

  return fuse.search(value).slice(0, 5);
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { height: 250, flexGrow: 1 },
    container: { position: 'relative' },
    suggestionsContainerOpen: { position: 'absolute', zIndex: 1, marginTop: theme.spacing(1), left: 0, right: 0 },
    suggestion: { display: 'block' },
    suggestionsList: { margin: 0, padding: 0, listStyleType: 'none' },
    divider: { height: theme.spacing(2) }
  })
);

export const AutoComplete: FC<AutoCompleteProps> = ({ label, suggestions, onSelected, placeHolder, clearable }) => {
  const classes = useStyles();
  const [state, setState] = useState({ newValue: '' });
  const [stateSuggestions, setSuggestions] = useState<string[]>([]);

  const handleChange = (e: ChangeEvent<{}>, { newValue }: Autosuggest.ChangeEvent) => setState({ ...state, newValue });
  const handleSuggestionsFetchRequested = ({ value }: any) => setSuggestions(getSuggestions(suggestions, value));
  const handelSuggestionSelected = (e: FormEvent<any>, { suggestion }: SuggestionSelectedEventData<any>) => onSelected(suggestion);
  const handleSuggestionsClearRequested = () => {
    if (state.newValue === '' && clearable === true) onSelected({ name: '' });
    setSuggestions([]);
  };

  const autosuggestProps = {
    renderInputComponent,
    suggestions: stateSuggestions,
    onSuggestionsFetchRequested: handleSuggestionsFetchRequested,
    onSuggestionsClearRequested: handleSuggestionsClearRequested,
    onSuggestionSelected: handelSuggestionSelected,
    renderSuggestion,
    highlightFirstSuggestion: true,
    getSuggestionValue: val => val.name
  };

  return (
    <Autosuggest
      {...autosuggestProps}
      inputProps={{
        classes,
        label,
        placeholder: placeHolder || `Search a ${label.toLowerCase()}`,
        value: state.newValue,
        onChange: handleChange
      }}
      theme={{
        container: classes.container,
        suggestionsContainerOpen: classes.suggestionsContainerOpen,
        suggestionsList: classes.suggestionsList,
        suggestion: classes.suggestion
      }}
      renderSuggestionsContainer={options => (
        <Paper {...options.containerProps} square>
          {options.children}
        </Paper>
      )}
    />
  );
};
