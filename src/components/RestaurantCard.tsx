import React, { FC } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Card, CardContent, CardMedia, Divider, Button, Typography, Box } from '@material-ui/core';
import { Phone } from '@material-ui/icons';
import { IRestaurant } from '../types';
import { printDollarSigns, removePhoneExtention } from '../helpers';

const useStyles = makeStyles(() =>
  createStyles({
    card: { margin: '15px 0', display: 'flex' },
    details: { display: 'flex', flex: '1', flexDirection: 'column' },
    content: { flex: '1 0 auto' },
    cover: { width: 151 }
  })
);

export const RestaurantCard: FC<IRestaurant> = ({ name, address, city, postal_code, area, price, phone, image_url }) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardMedia className={classes.cover} image={image_url} title={name} />
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h5" variant="h5">
            {name}
          </Typography>
          <Box display="flex">
            <Box flexGrow={1}>
              <Typography variant="subtitle1" color="textSecondary">
                {area}
              </Typography>
              <Typography variant="subtitle2" color="textSecondary">
                {address}, {city} {postal_code}
              </Typography>
            </Box>
            <Box>{printDollarSigns(price)}</Box>
          </Box>
        </CardContent>
        <Divider variant="middle" />
        <Button href={'tel:' + removePhoneExtention(phone)} size="large" fullWidth>
          <Phone />
          {removePhoneExtention(phone)}
        </Button>
      </div>
    </Card>
  );
};
