import React from 'react';
import { AutoComplete } from './AutoComplete';
import { shallow } from 'enzyme';

it('renders without crashing', () => {
  const props = { label: 'test', suggestions: [{ label: 'testing' }] };

  shallow(<AutoComplete {...props} onSelected={() => {}} />);
});
