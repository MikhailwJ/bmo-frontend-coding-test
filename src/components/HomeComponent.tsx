import React, { Component } from 'react';
import { AutoComplete } from './AutoComplete';
import { connect } from 'react-redux';
import { requestCities, requestCountries } from '../redux/actions/locations';
import { requestRestaurants, filterRestaurants } from '../redux/actions/restauraunts';
import { Box, Container } from '@material-ui/core';
import { Header } from './Header';
import { RestaurantCard } from './RestaurantCard';
import { IRestaurant, SelectedLocation } from '../types';

class HomeComponentImpl extends Component<any, any> {
  componentDidMount() {
    this.props.requestCities();
    this.props.requestCountries();
  }

  fetchRestaurants = (location: SelectedLocation) => this.props.requestRestaurants(location.name);
  filterRestaurants = (location: SelectedLocation) => this.props.filterRestaurants(location);

  render() {
    const { items: data, filtered } = this.props.restaurants;
    const { items, addresses, areas } = data.reduce(
      (acc, restaurant: IRestaurant) => {
        const { area, address, name } = restaurant;
        acc.addresses.add(address);
        acc.areas.add(area);
        acc.items.push({ name, type: 'name' });
        return acc;
      },
      { items: [], addresses: new Set(), areas: new Set() }
    );
    const restaurants = [
      ...items,
      [...addresses].map(add => ({ name: add, type: 'address' })),
      [...areas].map(add => ({ name: add, type: 'area' }))
    ].flat();

    return (
      <Box>
        <Header title="Restaurants" />
        <Container style={{ marginTop: 15 }}>
          <AutoComplete label={'City'} suggestions={this.props.cities.items} onSelected={this.fetchRestaurants} />
          {restaurants.length > 0 && (
            <AutoComplete
              label={'Refine'}
              placeHolder={'Refine search'}
              suggestions={restaurants}
              onSelected={this.filterRestaurants}
              clearable={true}
            />
          )}
          <Box my={4}>
            {(filtered || data).map(({ id, ...restaurant }) => (
              <RestaurantCard {...restaurant} key={id} />
            ))}
          </Box>
        </Container>
      </Box>
    );
  }
}

const mapStateToProps = state => ({ cities: state.cities, countries: state.countries, restaurants: state.restaurants });

const mapDispatchToProps = dispatch => ({
  requestCities: () => dispatch(requestCities()),
  requestCountries: () => dispatch(requestCountries()),
  requestRestaurants: (location: string) => dispatch(requestRestaurants(location)),
  filterRestaurants: (location: SelectedLocation) => dispatch(filterRestaurants(location))
});

export const HomeComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeComponentImpl);
